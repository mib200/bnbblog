=== Google Analytics Adder With User Id Tracking ===
Contributors: jeffbullins
Donate link: http://www.thinklandingpages.com
Tags:  analytics, google, google analytics, statistics, stats, tracking, analytics, display advertising, remarketing analytics,tracking code, universal analytics, GA code, GA code integration, GA Plugin, GA Script, Google Analytics in WordPress, universal analytics, WordPress Google Analytics, WP Google Analytics, WP Google Analytics Plugin
Requires at least: 3.0
Tested up to: 3.8.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily add your Google Analytics to your Wordpress site.  Use classic analytics code or Google's new Univeral Analytics.  If you use Universal Analytics, the plugin will allow you to use Google's User Id tracking.

== Description ==

Easily add your Google Analytics to your Wordpress site.  Use classic Google analytics code or Google's new Universal Analytics.  If you use Google Universal Analytics, the plugin will allow you to use Google's User Id tracking.

<a href="http://www.thinklandingpages.com/landingpage/track-engage/">Check out TrackEngage</a> to learn how you can enhance Google Analytics.

###What you get when you use the Wordpress Google Analytics plugin

*  Use classic Google analytics code
*  Use Google's Universal Analytics code
*  Use Google's new User Id Tracking with Universal Analytics

###Enhance Google Analytics

Google Analytics provides good aggregate data for you to analyze the traffic coming to your site.  Google Analytics, like many analytics programs, doesn't provide you will analytics about how individual users are using your site.

We are providing this plugin to help you get Google Analytics working on your site.  We also use Google Analytics on our sites.

We also want to share a new service we are building that will enhance Google Analytics and help you be more successful in your business.

<a href="http://www.thinklandingpages.com/landingpage/track-engage/">Check out TrackEngage</a> to learn more about how we are making analytics better.

== Installation ==


1. Upload `contact-form` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Click **Google Analytics Adder** on the admin menu to enable and set your options.


== Frequently Asked Questions ==

= Do I have to know how to program or design? =

No.  The plugin does the programming and design.



== Changelog ==

= 1.0 =
* First Release


