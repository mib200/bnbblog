<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form" id="search-form" method="get" role="search">
	
	<!--<fieldset class="no-bot-marg">-->
		<label><img src="<?php echo get_template_directory_uri(); ?>/theme/images/search-icon.jpg" alt="search icon"></label>
		<?php /*?><legend for="comment" class="show-old"><?php _e('Type & hit enter to search','framework'); ?></legend><?php */?>
	
		<input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="<?php _e('Search','framework'); ?>" />
	
	<!--</fieldset>-->

</form>
