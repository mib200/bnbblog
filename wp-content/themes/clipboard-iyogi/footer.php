						<div class="clear"></div>
					
					</div><!-- end .rightContent -->

				<?php // copyright

				$copy_text=get_option('vk_copy_text');

				if($copy_text!='') { ?>

					<div class="clear"></div>

					<div class="copyright"><p style="font-size: 90%;"><?php echo $copy_text; ?></p></div>

				<?php } ?>

				<div class="clear"></div>

			</div><!-- end rightPadding -->

			<!-- iOS background helper -->
			<div class="iosBackground"></div>

		</div><!-- end rightContainer -->

		<span id="directoryRef" data-directory="<?php echo get_template_directory_uri(); ?>" data-loading="<?php _e('LOADING','framework'); ?>"></span>

	</div><!-- end #globalWrap -->

	<?php wp_footer(); ?>
<script type="text/javascript">
jQuery(document).ready(function(e) {
	jQuery('#search-form').on('click',function(e){
		e.stopPropagation();
		jQuery('.search-form').animate({
			width:170,
			}, 500, function() {
		});
		jQuery('.search-form').css({'border':'1px solid #999','transform':'width 400ms ease 0s, background 400ms ease 0s'});
		jQuery('.search-form > input[type=text]').css({'left':'24px'});
	});
	jQuery(document).click(function(){
		jQuery('.search-form').animate({
			width:20,
			}, 500, function() {
		});
		jQuery('.search-form').css({'border':'none','padding':'1px'});
		jQuery('.search-form > input[type=text]').css({'left':'150px'});
	});
	
});
</script>
</body>
</html>
<?php

	// create the output string
	$output = ob_get_contents();

	// end object
	ob_end_clean();

	// remove javascript comments
	$output = preg_replace('/(?<!\S)\/\/\s*[^\r\n]*/', '', $output);

	// remove whitespace
	$output = join("\n", array_map("trim", explode("\n", $output)));

	// remove tab spaces
	$output = preg_replace('/	/', '', $output);

	// remove double spaces (create single space)
	$output = preg_replace('/  /', ' ', $output);

	// remove empty lines
	$output = preg_replace('/\n+/', " ", trim($output));

	// compressed
	echo $output;

?>