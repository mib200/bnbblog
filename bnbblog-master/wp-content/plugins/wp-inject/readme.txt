﻿=== WP Inject ===
Contributors: thoefter
Tags: insert, wpinject, pictures, flickr, api, images, editor, photos, photo, image, inject, creative commons, creative, commons, gallery, media, thumbnail, seo, pixabay
Tested up to: 3.8.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily insert images and photos into your blog posts! WP Inject searches the huge Flickr database for creative commons photos related to any topic.

== Description ==

[WP Inject](http://wpinject.com/ "Free plugin to insert images into WordPress posts") allows you to **easily insert photos and images into your WordPress posts**. Search among many thousands of creative commons images for any keywords of your choice, then pick the best of them for the article you are working on and insert them into your post or set them as featured image! Best of all it is super fast: Injecting an image only takes 3 clicks in total!

No further setup is required after installing WP Inject. You can start inserting photos right away! To do so the plugin adds an easy to use search metabox to your WordPress editor ("Add New" post screens). Simply enter any keyword to find great photos for your post. 

Advanced users can head to the WP Inject settings page and fine tune the plugin. A lot of different options let you control most aspects of how WP Inject finds and inserts images into your posts. By editing the templates you can control exactly how the photos and automatic attribution will look on your blog!

**Have a look at my [tutorial on how to install and use WP Inject](http://wpinject.com/tutorial/ "How to insert images with WP Inject") to get started with the plugin.** Besides explaining the basics the tutorial also contains details on all the available settings in WP Inject and how the plugin works together with WordPress SEO by Yoast. 

= Supported Image Sources: =
*  **Flickr** - Over 200 million creative commons images. Attribution is automatically added where required.
*  **Pixabay** - More than 150,000 high quality public domain photos. No attribution required for any of them!
*  More sources will be added in the future, including support for injecting videos, ads and other types of content!

= Features: =
*  Search thousands of creative commons photos and include any into your posts.
*  Fast and easy: Inserting images takes 3 clicks and less than a minute!
*  Automatically adds the required attribution links next to the image.
*  Set the featured image for your post with a single click.
*  Insert multiple images at once and create whole galleries! 
*  Choose between several image sizes easily.
*  Modify the templates of WP Inject to change how images get displayed in your posts.
*  Images are saved to your own server and added to the WordPress media library.
*  Automatically populated ALT and title tags of the image for search engine optimization.
*  Search for many different keywords and compare the results to find the best matches.
*  Can use and insert your focus keyword set in WordPress SEO by Yoast for easier image search optimization.


I will update WP Inject regularly. The first feature update was released only a week after the initial launch of WP Inject and more will certainly follow soon! 

If you find any bugs please report them! General feedback or feature requests are also welcome. 


== Changelog ==  

= 1.00 =
- Adds Pixabay as an additional image source, providing over 150,000 high quality photos for your posts. To use Pixabay you need to register for a free account at pixabay.com and enter your API details on the WP Inject "Settings" page.

= 0.53 =
- Adds possibility to get more than 100 images per search to the Settings page
- Adds image title, author and date below previews of photos
- Fixes a small bug that could cause image uploads to fail.

= 0.52 =
- Improved error reporting for if the upload of images to the server fails.
- Fallback function used if image upload fails.
- Fixes a bug preventing the saving of photos for certain users.

= 0.51 =
- Fixes a bug that caused a fatal error on network activation on WP multisites.
- Fixes a minor bug with attribution links.

= 0.50 =
- Adds support to save multiple images to your media libary with one click
- Adds WP multisite support: You can now use "network activate" to activate WP Inject on all your blogs at the same time

= 0.41 =
- Fixes a bug that prevented one of the "Save Settings" buttons on the options page from working.

= 0.40 =
- Adds "Filename Template" setting, allowing you to customize the filename of saved images, e.g. for SEO reasons.
- Fixes a display bug on the Settings page for certain browsers.

= 0.31 =
- Adds a small CC icon to attribution text linking to the photo license.
- New tags for the attribution template: {cc_icon}, {license_name}, {license_link}

= 0.30 =
- New attribution template setting.
- New attribution location setting.
- Now uses WordPress own CSS classes for aligning inserted images.

= 0.20 =
- First public release.

== Screenshots ==

1. The metabox is added to the WordPress editor page by WP Inject and allows you to search for photos fast.

2. Photo search results. Hover over any thumbnail and click on one of the size links to insert the photo into your post immediately.

3. You can select multiple photos by clicking on them and then insert them all together.

4. Support for multiple searches. Compare results side by side to find the best matching photos for your post.

== Installation ==

1. Upload the plugin folder to your /wp-content/plugins/ directory
2. Go to the "Plugins" page in your WordPress admin and activate WP Inject
3. Go to the "Add New" post or page screen in your WordPress admin and you will find a new metabox for WP Inject that allows you to easily insert images.

You can read my tutorial for more details on [how to install and use WP Inject](http://wpinject.com/tutorial/ "How to insert images with WP Inject").